#ifndef IOTP_CONNECTIVITY_H
#define IOTP_CONNECTIVITY_H
#include <iotp_device.h>

// main struct that contains the uci settings for the could conf
struct iotp_conn_settings {
	char *org_id;
	char *device_type;
	char *device_id;
	char *auth_token;
} iotp_conn_settings;

// initializes the iotp lib, connects to the ibm cloud
void init_iotp(struct iotp_conn_settings *setting, IoTPConfig **config, IoTPDevice **device);

// disconnects from the cloud and frees the structs involved
void iotp_cleanup(IoTPConfig *config, IoTPDevice *device);

// reads settings from uci
struct iotp_conn_settings read_conn_uci_settings();

// debug printout of `iotp_conn_settings`
void debug_iotp_print_conn_settings(struct iotp_conn_settings setting);

#endif /* IOTP_CONNECTIVITY_H */