#include <iotp_device.h>
#include <syslog.h>
#include <libtlt_uci.h> // :^)
#include <uci.h>

#include "iotp_connectivity.h"

void throw_on_bad_rc(int rc, IoTPConfig *config, IoTPDevice *device, char *message) {
    if (rc == IOTPRC_SUCCESS) {
        return;
    } else {
        if (message != NULL && strcmp(message, "") != 0) {
            syslog(LOG_ERR, message);
        } else {
            syslog(LOG_ERR, "Critical error on IBM library call.");
        }
        iotp_cleanup(config, device);
        exit(1);
    }
} 

void iotp_cleanup(IoTPConfig *config, IoTPDevice *device) {
    int rc;
    // Disconnect, and cleaup
    rc = IoTPDevice_disconnect(device);
    if ( rc == IOTPRC_SUCCESS ) {
        // Destroy client 
        rc = IoTPDevice_destroy(device);
        if ( rc == IOTPRC_SUCCESS ) {
            // Clear configuration
            rc = IoTPConfig_clear(config);
        }
    }
}

void init_iotp(struct iotp_conn_settings *setting, IoTPConfig **config, IoTPDevice **device) {
    int rc;

    // i think that this is bad code
    rc = IoTPConfig_create(config, ""); // call behaviour is to not do anything when no file is provided
    throw_on_bad_rc(rc, *config, *device, NULL);

    // set config values from uci conf struct
    rc = IoTPConfig_setProperty(*config, IoTPConfig_identity_orgId, setting->org_id);
    throw_on_bad_rc(rc, *config, *device, NULL);
    rc = IoTPConfig_setProperty(*config, IoTPConfig_identity_typeId, setting->device_type);
    throw_on_bad_rc(rc, *config, *device, NULL);
    rc = IoTPConfig_setProperty(*config, IoTPConfig_identity_deviceId, setting->device_id);
    throw_on_bad_rc(rc, *config, *device, NULL);
    rc = IoTPConfig_setProperty(*config, IoTPConfig_auth_token, setting->auth_token);
    throw_on_bad_rc(rc, *config, *device, NULL);

    rc = IoTPDevice_create(device, *config);
    throw_on_bad_rc(rc, *config, *device, NULL);

    rc = IoTPDevice_connect(*device);
    throw_on_bad_rc(rc, *config, *device, NULL);
}

struct iotp_conn_settings read_conn_uci_settings() {
    struct iotp_conn_settings output;

	struct uci_context *ctx = uci_init();


	if (!ctx) {// err chk
		perror("Out of memory!\n");
		syslog(LOG_ERR, "Ran out of memory!");
        exit(1);
	}

    output.org_id = ucix_get_option(ctx, "watson_status", "conf", "conn_org_id");
    output.device_type = ucix_get_option(ctx, "watson_status", "conf", "conn_device_type");
    output.device_id = ucix_get_option(ctx, "watson_status", "conf", "conn_device_id");
    output.auth_token = ucix_get_option(ctx, "watson_status", "conf", "conn_auth_token");
	
    return output;
}

void debug_iotp_print_conn_settings(struct iotp_conn_settings setting) {
    printf("\niotp_conn_settings:\norg_id: %s\ndevice_type: %s\ndevice_id: %s\nauth_token: %s\n",
        setting.org_id,
        setting.device_type,
        setting.device_id,
        setting.auth_token
    );
}