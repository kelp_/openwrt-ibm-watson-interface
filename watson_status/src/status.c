#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <syslog.h>
#include <stdlib.h>
#include <ubus_common.h>
#include <libubox/blobmsg_json.h>
#include <libubus.h>

#include "status.h"

struct mem_cb_info {
    int memory_type;
    int output_in_bytes;
} mem_cb_info;

enum MEMORY_TYPE {
	TOTAL_MEMORY,
	FREE_MEMORY,
	SHARED_MEMORY,
	BUFFERED_MEMORY,
    AVAILABLE_MEMORY,
	__MEMORY_MAX,
};

enum {
	MEMORY_DATA,
	__INFO_MAX,
};

static const struct blobmsg_policy memory_policy[__MEMORY_MAX] = {
	[TOTAL_MEMORY] = { .name = "total", .type = BLOBMSG_TYPE_INT64 },
	[FREE_MEMORY] = { .name = "free", .type = BLOBMSG_TYPE_INT64 },
	[SHARED_MEMORY] = { .name = "shared", .type = BLOBMSG_TYPE_INT64 },
	[BUFFERED_MEMORY] = { .name = "buffered", .type = BLOBMSG_TYPE_INT64 },
    [AVAILABLE_MEMORY] = { .name = "available", .type = BLOBMSG_TYPE_INT64 },
};

static const struct blobmsg_policy info_policy[__INFO_MAX] = {
	[MEMORY_DATA] = { .name = "memory", .type = BLOBMSG_TYPE_TABLE },
};





int get_avail_ram() {
    return get_memory_usage_in_bytes(AVAILABLE_MEMORY)/1000000;
}

int get_used_ram() {
    return (get_memory_usage_in_bytes(TOTAL_MEMORY)-get_memory_usage_in_bytes(AVAILABLE_MEMORY))/1000000;
}

int get_total_ram() {
    return get_memory_usage_in_bytes(TOTAL_MEMORY)/1000000;
}

int get_memory_usage_in_bytes(int type) {
    struct ubus_context *ctx;
	uint32_t id;

    // setup ubus
	ctx = ubus_connect(NULL);
	if (!ctx) {
		syslog(LOG_ERR, "Failed to connect to ubus!\n");
		return -1;
	}

    struct mem_cb_info info;
    info.memory_type = type;
    info.output_in_bytes = 0;

	if (ubus_lookup_id(ctx, "system", &id) ||
	    ubus_invoke(ctx, id, "info", NULL, meminfo_cb, &info, 3000)) {
        syslog(LOG_ERR, "Cannot request memory info from procd!\n");
	}

	ubus_free(ctx);

    return info.output_in_bytes;
}

static void meminfo_cb(struct ubus_request *req, int type, struct blob_attr *msg) {
    
    struct mem_cb_info* info = (struct mem_cb_info*)req->priv;

	struct blob_attr *tb[__INFO_MAX];
	struct blob_attr *memory[__MEMORY_MAX];

	blobmsg_parse(info_policy, __INFO_MAX, tb, blob_data(msg), blob_len(msg));

	if (!tb[MEMORY_DATA]) {
		syslog(LOG_ERR, "No memory data received!\n");
		return;
	}

	blobmsg_parse(memory_policy, __MEMORY_MAX, memory,
    blobmsg_data(tb[MEMORY_DATA]), blobmsg_data_len(tb[MEMORY_DATA]));

    info->output_in_bytes = blobmsg_get_u64(memory[info->memory_type]);
}
