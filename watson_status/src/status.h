#ifndef STATUS_H
#define STATUS_H

// returns a string with the available ram on the system
int get_avail_ram(void);

// returns a string with the currently used amount of ram on the system
int get_used_ram(void);

// returns a string with the total ram currently on the system
int get_total_ram(void);

// gets memory usage of specific type in bytes || INTERNAL USE ONLY
int get_memory_usage_in_bytes(int type);

// callback for memory info ubus poll
static void meminfo_cb(struct ubus_request *req, int type, struct blob_attr *msg);

#endif /*STATUS_H*/