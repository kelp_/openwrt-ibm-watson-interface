#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <syslog.h>
#include <uci.h>
#include <iotp_device.h>
#include <ubus_common.h>
#include <libubus.h>

#include "status.h"
#include "consts.h"
#include "iotp_connectivity.h"

volatile sig_atomic_t deamonize = 1;

void term_proc(int sigterm)
{
	deamonize = 0;
}

int main(void)
{
    char *event_data[250] = {'\0'}; // buffer string for json/mqtt event data

    // ubus vars
    struct ubus_context *uctx;

	// IoTP vars
	IoTPConfig *config = NULL;
    IoTPDevice *device = NULL;
    int rc;

	// logger init
	openlog("watson_status", LOG_INFO, 0);
    syslog(LOG_INFO, "Init");

	// setting init
	struct iotp_conn_settings settings = read_conn_uci_settings();

    // IoTP init
	init_iotp(&settings, &config, &device);

	// daemon init
	struct sigaction action;
	memset(&action, 0, sizeof(struct sigaction));
	action.sa_handler = term_proc;
	sigaction(SIGTERM, &action, NULL);

    syslog(LOG_INFO, "Daemon init successful, entering main loop");

	while(deamonize) { // main daemon loop

        // clear message buffer
        memset(event_data, '\0', sizeof(event_data));

        // send data into message buffer 
        snprintf(
            event_data,
            sizeof(event_data),
            "{\"mem_status\" : {\"used\": \"%dMB\", \"avail\": \"%dMB\", \"total\": \"%dMB\" }}",
            get_used_ram(),
            get_avail_ram(),
            get_total_ram()    
        );

        // send message buffer into IoT platform
        rc = IoTPDevice_sendEvent(device,"status", event_data, "json", QoS0, NULL);
        if ( rc != IOTPRC_SUCCESS ) { // errchk
            syslog(LOG_ERR, "Failed to publish event. rc=%d", rc);
        }

		sleep(5);// TOOD: possibly move sleep into config?
	}

    // loop done, begin cleanup
    iotp_cleanup(config, device);
	closelog(); // might need to closelog() in child process too, not yet tested
	return 0;
}
