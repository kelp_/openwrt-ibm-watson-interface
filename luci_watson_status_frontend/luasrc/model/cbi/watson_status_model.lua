map = Map("watson_status", "Watson Status")

section = map:section(NamedSection, "conf", "config", "Config")

flag = section:option(Flag, "enable_daemon", "Enable", "Enable program")

conn_org_id = section:option(Value, "conn_org_id", "Organization ID")
conn_org_id.datatype = "string"
conn_device_type = section:option(Value, "conn_device_type", "Device Type")
conn_device_type.datatype = "string"
conn_device_id = section:option(Value, "conn_device_id", "Device ID")
conn_device_id.datatype = "string"
conn_auth_token = section:option(Value, "conn_auth_token", "Auth token")
conn_auth_token.datatype = "string"

return map
